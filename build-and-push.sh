#!/bin/bash

repo="aicacia/docker-kube-helm"
set -e

function build_and_push() {
  local version=$1
  docker build -t ${repo}:${version} .
  docker push ${repo}:${version}
  docker tag ${repo}:${version} ${repo}:latest
  docker push ${repo}:latest
}

build_and_push "19.03-1.17-3.1.2"