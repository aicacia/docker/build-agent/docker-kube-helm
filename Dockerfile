# aicacia/docker-kube-helm:latest

FROM ubuntu:18.04

RUN apt update && \
  apt upgrade -y
RUN apt install -y \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg-agent \
  software-properties-common

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
  add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" && \  
  curl https://packages.cloud.google.com/apt/doc/apt-key.gpg -s | apt-key add - && \
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list

RUN apt update

ARG DOCKER_VERSION=19.03

RUN APT_VERSION=$(apt-cache madison docker-ce | grep "${DOCKER_VERSION}" | head -1 | awk '{print $3}') && \
  apt install -y \
  docker-ce=${APT_VERSION} \
  docker-ce-cli=${APT_VERSION} \
  containerd.io
RUN apt-mark hold docker-ce docker-ce-cli

ARG KUBERNETES_VERSION=1.17

RUN APT_VERSION=$(apt-cache madison kubeadm | grep ${KUBERNETES_VERSION} | head -1 | awk '{print $3}') && \
  apt install -y \
  kubelet=${APT_VERSION} \
  kubeadm=${APT_VERSION} \
  kubectl=${APT_VERSION}
RUN apt-mark hold kubelet kubeadm kubectl

RUN apt upgrade -y && \
  apt autoremove -y && \
  apt autoclean -y && \
  apt clean -y

ARG HELM_VERSION=3.1.2

RUN curl -s https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz -o helm.tar.gz && \
  tar xf helm.tar.gz && \
  mv linux-amd64/helm /usr/local/bin/ && \
  rm -rf linux-amd64 && \
  rm helm.tar.gz

ARG HELM_PUSH_URL=https://github.com/chartmuseum/helm-push
RUN helm plugin install ${HELM_PUSH_URL}

ENTRYPOINT ["/bin/bash"]